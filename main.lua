local gamera = require("lib.gamera")
local imgui = require("imgui")
require("lib.engine")
require("lib.stacktrace")
tileNames = {}

local tile = {}
local tiles = {}
local map = {}
local info = {}
local mapW = 64
local mapH = 64
local mapX = 0
local mapY = 0
local tileW = 32
local tileH = 32



local mx, my = 0, 0
local mxw, myw = 0, 0
local txw, tyw = 0, 0

local tileid = 1


local cam = gamera.new(0,0,2000,2000)

-- CAPUTRE INPUT
require("lib.editor.input")

-- Engine modules
local tilesUtils = require("lib.editor.tiles")
local menuBar = require("lib.ui.titlemenu")
local mouseEventsListener = require("lib.editor.mouse_events")

-- Some functions in main.lua

function love.load()
	-- our tiles
	files = love.filesystem.getDirectoryItems("tiles")
		tile[0] = love.graphics.newImage("internal/none.png") -- loads intenal tile
		tilesUtils.add("internal/none.png", 0)
	   for i=1, #files do
			 --print(files[i])
	      tile[i] = love.graphics.newImage("tiles/"..files[i])
				tilesUtils.add("tiles/"..files[i])
	   end

		 map = generateMap(64, 64, 1, 400)
		 cam:setWorld(0,0,2000,2000)
		 cam:setWindow(0,0,800,600)

end

function love.draw()
	cam:draw(function(l,t,w,h)
  	drawMap(tile, map, 62, 62, mapX, mapY, tileW, tileH, -32, -32)

		love.graphics.setColor(0, 255, 0, 100)
		love.graphics.rectangle("line", math.floor( mxw / 32 ) * 32, math.floor( myw / 32 ) * 32, 32, 32)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.print(table.concat(info, " "), 1, 1)
	end)
	tileid, tiles = menuBar.draw(tileid, tiles)
	imgui.Render()
end

function love.update(dt)
	mx, my = love.mouse.getX(), love.mouse.getY()
  mxw, myw = cam:toWorld(mx,my)
	txw, tyw = math.floor( mxw / 32 ) * 32, math.floor( myw / 32 )
	info = {"mouse:", mx, my, "global:", mxw, myw, "tile:", txw, tyw}
	-- Mouse events
	mouseEventsListener.TileBuilding(mxw, myw, map, mapW, mapH, tileid, mapX, mapY, 0, 0)
	imgui.NewFrame()
end
