function saveUI()
 cannotBuild = false
 imgui.Begin("Load map", true)
 imgui.Text("Enter map name:")
 imgui.SameLine()
 mapname = imgui.InputText(".lm", tostring(mapname), 200, 300, 200)
 imgui.Separator()
 if imgui.Button("Cancel") then
	 windows[3] = false
	 cannotBuild = not cannotBuild
 end
 imgui.SameLine()
 if imgui.Button("Load") then
	 print("load: OK!")
	 loadEngine(mapname .. ".lm")
	 windows[3] = false
	 cannotBuild = not cannotBuild
 end
end
