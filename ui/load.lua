function loadUI()
 cannotBuild = false
 imgui.Begin("Save map", true)
 imgui.Text("Enter map name:")
 imgui.SameLine()
 mapname = imgui.InputText(".lm", tostring(mapname), 200, 300, 200)
 imgui.Separator()
 if imgui.Button("Cancel") then
	 windows[2] = false
	 cannotBuild = not cannotBuild
 end
 imgui.SameLine()
 if imgui.Button("Save") then
	 print("save: OK!")
	 saveEngine(mapname, ent, posx, posy)
	 windows[2] = false
	 cannotBuild = not cannotBuild
 end
end
