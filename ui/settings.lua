function settingsUI()
	 imgui.Begin("Settings", false)
	 imgui.Text("Map editor settings:")
	 imgui.Separator()
	 imgui.Text(string.format("Window resolution (Current: %s x %s):", tostring(resolutionX), tostring(resolutionY)))
	 resolutionX = imgui.InputText("Window height", tostring(resolutionX), 200, 300, 200)
	 resolutionX = tonumber(resolutionX)
	 resolutionY = imgui.InputText("Window width", tostring(resolutionY), 200, 300, 200)
	 resolutionY = tonumber(resolutionY)
	 if imgui.Button("Apply screen changes") then
		love.window.setMode(resolutionX, resolutionY)
		cam:setWindow(0,0,resolutionX,resolutionY)
	 end
	 imgui.Separator()

	 imgui.Text("Editor settings")
	 zoomfactor = imgui.InputText("Zoom factor", tostring(zoomfactor), 200, 300, 200)
	 reloadfactor = imgui.InputText("Insert time", tostring(reloadfactor), 200, 300, 200)
	 camspeed = imgui.InputText("Cam speed", tostring(camspeed), 200, 300, 200)
	 insertCount = imgui.InputText("Random insert count", tostring(insertCount), 200, 300, 200)
	 gridx = imgui.InputText("Grid size X", tostring(gridx), 200, 300, 200)
	 gridy = imgui.InputText("Grid size Y", tostring(gridy), 200, 300, 200)

	 -- I do not know why, but without this nothing works
	 zoomfactor = tonumber(zoomfactor)
	 gridy = tonumber(gridy)
	 gridx = tonumber(gridx)
	 camspeed = tonumber(camspeed)
	 reloadfactor = tonumber(reloadfactor)
	 insertCount = tonumber(insertCount)

	 imgui.Separator()
	 imgui.Text("Graphics and perfomance (WARNING: These settings can greatly affect the performance!)")
	 local ena2 = imgui.Checkbox("Enable sprite clipping", enabledSpriteClip)
	 enabledSpriteClip = ena2
	 if imgui.Button("Close") then
		 closeCurrentUI()
	 end
end
