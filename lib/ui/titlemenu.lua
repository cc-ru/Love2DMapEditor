local keyCombo = require("lib.editor.keycombo")
local tilesUtils = require("lib.editor.tiles")

local titleMenu = {}

function titleMenu.draw(tileid)
	titles = {}
	if imgui.BeginMainMenuBar() then
			if imgui.BeginMenu("File")  then
					if imgui.MenuItem("New", keyCombo.printCombo('newFile')) then
						print("fad")
						titles = {}
						titles = generateMap(64, 64, 0, 0)
					end
					imgui.Separator()
					imgui.MenuItem("Save", keyCombo.printCombo('saveFile'))
					imgui.MenuItem("Load", keyCombo.printCombo('loadFile'))
				 	if imgui.MenuItem("Exit") then os.exit() end
					imgui.EndMenu()
			end
			if imgui.BeginMenu("Editor") then
				if imgui.MenuItem("Reset pos") then os.exit() end
				imgui.EndMenu()
			end
			if imgui.BeginMenu("Objects") then
				local names = tilesUtils.get()
				for i = 1, #names do
					if i == tileid then
						imgui.MenuItem(tostring(i..":"..names[i].." (Selected)"))
					else
						if imgui.MenuItem(tostring(i..":"..names[i])) then tileid = i print("pressed!") end
					end
				end
				imgui.EndMenu()
			end
			imgui.EndMainMenuBar()
	end
	return tileid, titles
end

return titleMenu
