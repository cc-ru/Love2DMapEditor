resolutionX = 800
resolutionY = 600
selected = 1
mapsize = 0
reloadfactor = 0.01
zoomfactor = 0.1
camspeed = 800
enabledSpriteClip = true
reloadfactor = 0.01
insertCount = 1000
gridx = 32
gridy = 32

imguiWindows = {
	{false, settingsUI},
	{false, saveUI},
	{false, loadUI},
	{false, objectInspectorUI},
	{false, testImguiUI}
}
