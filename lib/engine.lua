function CheckColls(x1,y1,w1,h1, x2,y2,w2,h2)
 if x1 and x2 and w1 and w2 and x2 and y2 and w2 and h2 then
  return x1 < x2 + w2 and
  x2 < x1 + w1 and
  y1 < y2 + h2 and
  y2 < y1 + h1
 else
  return false
 end
end

function drawMap(tile, map, mapDisplayH, mapDisplayW, mapX, mapY, tileW, tileH, mapOffsetX, mapOffsetY)
   for y=1, mapDisplayH do
      for x=1, mapDisplayW do
         love.graphics.draw(
            tile[map[y+mapY][x+mapX]],
            (x*tileW)+mapOffsetX,
            (y*tileH)+mapOffsetY)
      end
   end
end

function generateMap(w, h, min, max)
  print("generating map...")
	local map = {}
  for i = 1, w do
    local bs = {}
    for i = 1, h do
      table.insert(bs, math.random(min, max))
    end
    table.insert(map, bs)
  end
	return map
end
