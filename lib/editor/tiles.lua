local tilesUtils = {}

local tileNames = {}

function tilesUtils.add(tilename, id)
	if id then
		tileNames[id] = string.gsub(tilename, "%.(%S*)", "")
	else
		table.insert(tileNames, (string.gsub(tilename, "%.(%S*)", "")))
	end
end

function tilesUtils.get()
	return tileNames
end

return tilesUtils
