local keyCombo = {}

local keyCombos = {
		saveFile = {"lctrl", "s"},
		loadFile = {"lctrl", "o"},
		newFile = {"lctrl", "n"}
}

function keyCombo.getKey(keyc)
	if keyCombos[keyc] then
		return keyCombos[keyc]
	else
		return error("Failed to get combo key!")
	end
end

function keyCombo.printCombo(keyc)
	if keyCombos[keyc] then
		return keyCombos[keyc][1].."+"..keyCombos[keyc][2]
	else
		return error("Failed to get combo key!: "..keyc)
	end
end

return keyCombo
