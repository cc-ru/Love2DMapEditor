local mouseEvents = {}

function mouseEvents.TileBuilding(mxw, myw, map, w, h, tileid, mapX, mapY, mapOffsetX, mapOffsetY)
	--print(map, x, y, tileid, mapX, mapY)
	for y=1, h do
    for x=1, w do
      if (x*32)+mapOffsetX == math.floor(mxw / 32) * 32 and (y*32)+mapOffsetY == math.floor(myw / 32) * 32 then
				if love.mouse.isDown(1) then map[y+1][x+1] = tileid end
				if love.mouse.isDown(2) then map[y+1][x+1] = 0 end
      end
  	end
	end
end

return mouseEvents
