local function insertNonNil(t, v)
  if v then
    v = tostring(v)
    if #v > 0 then
      table.insert(t, v)
    end
  end
end

function traceback(c)
  for level = 0, math.huge do
    local info = debug.getinfo(c, level, "ufnSl")

    if not info then
      break
    end

    local funcType, name, args, exec, defined = {}, nil, {}, "", {}

    insertNonNil(funcType, info.what)
    insertNonNil(funcType, info.namewhat)
    table.insert(funcType, "function")

    name = info.name or "<anon>"

    if info.nparams then
      for an = 1, info.nparams do
        local argName, argValue = debug.getlocal(c, level, an)

        if argValue ~= nil then
          argName = argName .. "=" .. tostring(argValue)
        end

        table.insert(args, argName)
      end
    end

    if info.isvararg then
      table.insert(args, "...")
    end

    if info.currentLine then
      exec = ":" .. tostring(info.currentLine)
    end

    insertNonNil(defined, info.short_src)
    insertNonNil(defined, info.linedefined)

    funcType = table.concat(funcType, " ")
    args = table.concat(args, ", ")
    defined = (defined[1] and ("in " .. defined[1] .. " ") or "") ..
              (defined[2] and ("at " .. defined[2]) or "")

    -- local Lua function <anon>(a, b, c, ...):33 (defined in blah.lua at 33)

    local line = ("#%2d: %s %s%s%s%s"):format(
      level,
      funcType,
      name,
      #args > 0 and ("(" .. args .. ")") or "",
      exec,
      #defined > 0 and (" (defined " .. defined .. ")") or ""
    )

    print(line)
  end
end

-- stdout:
--> outer   42      24
--> # 0: C field function yield(...) (defined in [C] at -1)
--> # 1: Lua local function f(f=function: 0x55bd66704ef0, a=42, b=24, ...) (defined in trace.lua at 75)
--> # 2: Lua local function outer(f=function: 0x55bd66a98d10, g=function: 0x55bd66704ef0, a=42, b=24) (defined in trace.lua at 70)
--> # 3: Lua function <anon> (defined in trace.lua at 69)
--> inner   42      24
