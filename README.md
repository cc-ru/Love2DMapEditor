# Love2DMapEditor (WIP)
A simple map editor for love2d. Written on love2d and imgui. This a alpha version! Can save/load maps in `.lm` format

## Dependencies

1. Love-imgui

## Screenshots

![alt text](https://i.imgur.com/NQHaaTP.png "Image 1")

## Screenshots (Legacy version)

![alt text](https://i.imgur.com/8z3ysAL.png "Image 1")
![alt text](https://i.imgur.com/be7O1A2.png "Image 2")

